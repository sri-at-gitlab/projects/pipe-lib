# Pipe Lib

Curated list of reusable pipelines and jobs.

Pipe Lib demonstrates how a GitLab project can serve as a "package repository" for reusable pipelines and jobs.

## Using Pipe Lib

- Identify the base URL, for example:
    - `https://gitlab.com/sri-at-gitlab/projects/pipe-lib`
- Reusable pipelines library:
    - `${base_url}/-/raw/master/pipelines_library.json`
- Reusable jobs library:
    - `${base_url}/-/raw/master/jobs_library.json`
- Sources of library items:
    - `${base_url}/-/raw/master/${library_item.path}`

## Contributing Pipelines and Jobs

### 1. Create your feature branch and the associated Merge Request

### 2. Merge request title format:
```python
"Contrib - Pipeline - {title}"        # for pipelines
"Contrib - Job - {title}"             # for jobs
```

### 3. Add your definition:
```shell
vim ./pipelines/{name}.gitlab-ci.yaml # for pipelines
vim ./jobs/{name}.gitlab-ci.yaml      # for jobs
```
- Pipeline / job definition should work standalone
- Use Docker images for non-Yaml resources
- Explicitly define variables

### 4. Include your definition in root `.gitlab-ci.yml` as proof:
```yaml
include:
    local: ./pipelines/{name}.gitlab-ci.yaml

# or

include:
    local: ./pipelines/{name}.gitlab-ci.yaml
```
- This should trigger a pipeline against your MR
- Pipeline serves as an example

### 5. Add an entry in `./pipelines_library.json` or `./jobs_library.json`
```js
// pipelines_library.json
[
    {
        path: './pipelines/{name}.yaml',
        author_name: '{author_name}',
        author_email: '{author_email}',
        title: '{title}',
        description: '{description}',
        keywords: ['list', 'of', 'keywords', 'used', 'for', 'search'],
    }
]
```
```js
// jobs_library.json
[
    {
        path: './jobs/{name}.yaml',
        author_name: '{author_name}',
        author_email: '{author_email}',
        title: '{title}',
        description: '{description}',
        keywords: ['list', 'of', 'keywords', 'used', 'for', 'search'],
    }
]
```

### 6. Wait for maintainer to approve and merge

## FAQ

### How may I use / consume Pipe Lib?

- Web API ~August 2020

### My pipeline / job requires more files than a Yaml definition. How can I share it on Pipe Lib?

Short answer: Use containers.

Details:
- Create a container image with all the dependencies, files and resources.
- Publish it on Docker Hub or any container registry of your choice.
- After which, this image may be used as the `image` for the pipeline job.
